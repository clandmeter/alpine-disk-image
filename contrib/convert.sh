#!/bin/sh

set -eu

INPUT_FILE=${1:?Please provide input file}
readonly MB=$(( 1024 * 1024 ))

einfo() {
	printf '\n\033[1;36m> %s\033[0m\n' "$@" >&2
}

case ${INPUT_FILE##*.} in
	qcow2) true ;;
	gz) gunzip -k "$INPUT_FILE"; INPUT_FILE=${INPUT_FILE%.*};;
	xz) unxz -k "$INPUT_FILE"; INPUT_FILE=${INPUT_FILE%.*};;
	*) echo "Extension not supported"; exit 1;;
esac

einfo "Creating intermediate raw file"
qemu-img convert -f qcow2 -O raw "$INPUT_FILE" "${INPUT_FILE%.*}".raw

SIZE=$(qemu-img info -f raw --output json "${INPUT_FILE%.*}".raw |
	awk '/virtual-size/ {print $2-1}')

einfo "Resizing image to $SIZE bytes"
qemu-img resize -f raw "${INPUT_FILE%.*}.raw" \
	"$(((SIZE/MB + 1)*MB))" > /dev/null

einfo "Converting to vhd disk image"
qemu-img convert -f raw -o subformat=fixed,force_size -O vpc \
	"${INPUT_FILE%.*}".raw "${INPUT_FILE%.*}".vhd

einfo "Removing intermediate raw file"
rm -rf "${INPUT_FILE%.*}.raw"
