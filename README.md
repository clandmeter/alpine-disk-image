# Alpine bare metal disk image

## Description
This project is a set of scripts to generate Alpine Linux disk images in [qcow2](https://en.wikipedia.org/wiki/Qcow) format for both x86_64 and Aarch64 architectures. These images are produced with [packer](https://www.packer.io/) and should be as generic as possible. It's logic is similar to [Alpine Linux cloud images](https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images) except it is currently tailored towards bare metal. With generic this means it does not ship with cloud-init support and with bare metal it means it will ship by default with Alpine Linux LTS kernel instead of the virt kernel used in our official cloud images.

## Use case
The produced images are currently used for [Equinix Metal](https://metal.equinix.com/) but they should be able to be used by any bare metal (or cloud) product with limitations as previously mentioned.  This is in no way a replacement for Alpine Linux cloud images, as it is born out of need to support for bare metal images instead of cloud specific images.

## CI automated build
New images are automatically build upon Alpine Linux aports git tags. Whenever a new release (git tag) is created from the aports repository it will trigger the pipeline in this project.
To build these images special Gitlab runners have been installed to support Qemu with KVM support.
