#!/usr/bin/lua5.3

local http  = require("http.request")
local cjson = require("cjson")

local token = "__REDACTED__"
local uri = "https://gitlab.alpinelinux.org/api/v4/projects/1747/ref/master/trigger/pipeline"
local timeout = 10

local headers = cjson.decode(os.getenv("HOOK_HEADERS"))
local data = cjson.decode(os.getenv("HOOK_PAYLOAD"))

if data.project.path_with_namespace == "alpine/aports" and data.event_name == "tag_push" then
        local tag = data.ref:match("^refs/tags/(.*)")
        local req = http.new_from_uri(uri)
        req.headers:upsert(":method", "POST")
        req.headers:upsert("content-type", "multipart/form-data")
        req:set_body(("token=%s&variables[APORTS_TAG]=%s"):format(token, tag))
        local headers, stream = req:go(timeout)
        local body = assert(stream:get_body_as_string())
        if headers:get ":status" ~= "201" then
                error(body)
        end
end
