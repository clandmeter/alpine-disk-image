#!/bin/sh

set -eu

: "${BOOT_WAIT:=60s}"
: "${DISK_COMPRESSION:=false}"
: "${EXTRA_SYSTEM_PKGS:=}"
: "${ENABLE_ROOT:=false}"
: "${APORTS_TAG:=}"
: "${ARCH:=$(arch)}"

LATEST_VERSION=$(wget -qO- https://alpinelinux.org/releases.json | jq -r '.release_branches[1].releases[0].version')

# use requested tag as version or fallback to latest version
if [ "$APORTS_TAG" ]; then
	VERSION=${APORTS_TAG#*v}
else
	VERSION=$LATEST_VERSION
fi

case $ARCH in
	x86_64)  : "${QEMU_MACHINE:=pc}";;
	aarch64) : "${QEMU_MACHINE:=virt}";;
	*) echo "Unsupported arch: $ARCH"; exit 1;;
esac

# shellcheck source=/dev/null
[ -f "build.conf" ] && . ./build.conf

export BOOT_WAIT \
	DISK_COMPRESSION \
	EXTRA_SYSTEM_PKGS \
	LATEST_VERSION \
	VERSION \
	ARCH \
	APORTS_TAG \
	ENABLE_ROOT \
	QEMU_MACHINE

einfo() {
	printf '\n\033[1;36m> %s\033[0m\n' "$@" >&2  # bold cyan
}

### Main ###

einfo "Building image for version $VERSION"

einfo "Cleaning up"
rm -rf ssh http out

einfo "Generate temporary ssh key"
mkdir -p ssh http
ssh-keygen -q -t ed25519 -N "" -f ./ssh/id_ed25519
ln -sf ../ssh/id_ed25519.pub http/id_ed25519.pub

einfo "Running packer for $ARCH"
packer build alpine-generic.json

outfile="$(realpath out/*.qcow2)"

if [ -f "$outfile" ]; then
	einfo "Compressing image"
	xz $(xz -h 2>&1|grep -q threads && echo -T 0) "$outfile"
	einfo "Generating sha512 checksum"
	(cd out; sha512sum "${outfile##*/}.xz" > "${outfile##*/}".xz.sha512)
	echo "$VERSION" > out/.alpine-version
else
	einfo "No output file found!"
	exit 1
fi
