#!/bin/ash
# shellcheck shell=dash

set -eu

readonly MIRROR=https://dl-cdn.alpinelinux.org/alpine
readonly MOUNTPOINT=/mnt
readonly EFIDIR=boot/efi

: "${BIOS_PART_CMD:=}"
: "${INSTALLER_PKGS:=}"
: "${EXTRA_SYSTEM_PKGS:=}"
: "${ESP_PARTITION:=vda1}"
: "${ROOT_PARTITION:=vda2}"
: "${ENABLE_ROOT:=false}"
: "${SERIAL_DEV:=ttyS0}"

rc_add() {
	local runlevel="$1"; shift  # runlevel name
	local services="$*"  # names of services

	local svc; for svc in $services; do
		mkdir -p "$MOUNTPOINT"/etc/runlevels/"$runlevel"
		ln -s /etc/init.d/"$svc" "$MOUNTPOINT"/etc/runlevels/"$runlevel"/"$svc"
		echo " * service $svc added to runlevel $runlevel"
	done
}

einfo() {
	printf '\n\033[1;36m> %s\033[0m\n' "$@" >&2  # bold cyan
}

get_release() {
	local version=${1:-$(cat /etc/alpine-release 2>/dev/null)}
	local release=edge
	case "$version" in
		[0-9]*.[0-9]*.[0-9]*) release="v${version%.[0-9]*}";;
	esac
	echo "$release"
}

case "$(arch)" in
	x86_64)
		target=x86_64-efi
		fwa=x64
		INSTALLER_PKGS=grub-bios
		BIOS_PART_CMD="--new 0::+1M --typecode=0:ef02"
		ESP_PARTITION=vda2
		ROOT_PARTITION=vda3;;
	aarch64)
		target=arm64-efi
		fwa=aa64
		SERIAL_DEV=ttyAMA0;;
esac

einfo "Setting up repositories"
printf "$MIRROR/$(get_release)/%s\n" main community > \
	/etc/apk/repositories

einfo "Adding packages to installer"
apk add \
	sgdisk \
	e2fsprogs \
	dosfstools \
	grub \
	grub-efi \
	$INSTALLER_PKGS

einfo "Partitioning disk"
# shellcheck disable=SC2086
sgdisk --clear \
	$BIOS_PART_CMD \
	--new 0::+100M --typecode=0:ef00 \
	--new 0::-0 --typecode=0:8300 \
	/dev/vda

einfo "Create filesystems"
mkfs.ext4 -q /dev/"$ROOT_PARTITION"
mkfs.fat -F 32 /dev/"$ESP_PARTITION"

einfo "Loading filesystem drivers"
modprobe -a ext4 vfat

einfo "Mount image and bind mounts"
mount /dev/"$ROOT_PARTITION" "$MOUNTPOINT"
mkdir -p "$MOUNTPOINT"/boot/efi \
	"$MOUNTPOINT"/proc \
	"$MOUNTPOINT"/dev \
	"$MOUNTPOINT"/sys \
	"$MOUNTPOINT"/etc/default
mount /dev/"$ESP_PARTITION" "$MOUNTPOINT"/boot/efi
mount -t proc none "$MOUNTPOINT"/proc
mount --bind /dev "$MOUNTPOINT"/dev
mount --bind /sys "$MOUNTPOINT"/sys

einfo "Copy installer keys to image"
mkdir -p "$MOUNTPOINT"/etc/apk/keys
cp /etc/apk/keys/*.pub "$MOUNTPOINT"/etc/apk/keys/ 

einfo "Install grub default config"
cat > "$MOUNTPOINT"/etc/default/grub <<- EOF
	GRUB_TIMEOUT=2
	GRUB_DISABLE_SUBMENU=y
	GRUB_DISABLE_RECOVERY=true
	GRUB_CMDLINE_LINUX_DEFAULT="modules=ext4 console=tty0 console=$SERIAL_DEV,115200 quiet"
	EOF

if [ "$(arch)" = "x86_64" ]; then
	einfo "Install grub in bios mode"
	grub-install --boot-directory="$MOUNTPOINT"/boot \
		--target=i386-pc \
		/dev/vda
fi

einfo "Install grub in efi mode"
grub-install --target="$target" \
	--efi-directory="$MOUNTPOINT"/boot/efi \
	--bootloader-id=alpine \
	--boot-directory="$MOUNTPOINT"/boot \
	--no-nvram

# this is the default location when no nvram is used
install -D "$MOUNTPOINT"/"$EFIDIR"/EFI/alpine/grub${fwa}.efi \
	"$MOUNTPOINT"/"$EFIDIR"/EFI/boot/boot${fwa}.efi

einfo "Enable main and community for image"
# shellcheck disable=SC2153
printf "$MIRROR/$(get_release "$VERSION")/%s\n" main community > \
	"$MOUNTPOINT"/etc/apk/repositories

einfo "Creating system on image"
apk add \
	--root "$MOUNTPOINT" \
	--initdb \
	--update-cache \
	--clean-protected \
	alpine-base \
	linux-lts \
	grub \
	doas \
	openssh-server \
	$EXTRA_SYSTEM_PKGS

einfo "Enable doas for wheel group"
echo "permit nopass :wheel" > "$MOUNTPOINT"/etc/doas.d/wheel.conf

if [ "$ENABLE_ROOT" != "true" ]; then
	einfo "Disable the root user"
	chroot "$MOUNTPOINT" /usr/bin/passwd -l root
	chroot "$MOUNTPOINT" /bin/sh -c "echo 'root:*' | chpasswd -e"
fi

einfo "Setup system user alpine"
chroot "$MOUNTPOINT" /usr/sbin/adduser -D alpine
chroot "$MOUNTPOINT" /usr/sbin/adduser alpine wheel
chroot "$MOUNTPOINT" /bin/sh -c "echo 'alpine:*' | chpasswd -e"

einfo "Enable DHCP networking"
chroot "$MOUNTPOINT" /sbin/setup-interfaces -a

einfo "Setup needed services"
rc_add sysinit devfs dmesg mdev hwdrivers cgroups
rc_add boot modules hwclock swap hostname sysctl bootmisc syslog
rc_add shutdown killprocs savecache mount-ro
rc_add default networking ntpd sshd

if grep -q tiny-cloud "$MOUNTPOINT"/etc/apk/world; then
	einfo "Enable tiny cloud services"
	rc_add sysinit tiny-cloud-early
	rc_add default tiny-cloud tiny-cloud-final
fi

if ! grep -xq "$SERIAL_DEV" "$MOUNTPOINT"/etc/securetty; then
	einfo "Enable serial access on $SERIAL_DEV"
	echo "$SERIAL_DEV" >> "$MOUNTPOINT"/etc/securetty
fi

einfo "Enable getty on $SERIAL_DEV"
sed -e '/^#ttyS0:/s/^#//' -e "/^ttyS0/s/ttyS0/$SERIAL_DEV/g" \
	-i "$MOUNTPOINT"/etc/inittab

einfo "Cleaning up"
rm -rf "$MOUNTPOINT"/var/cache/apk/*

einfo "Finished"
